#!python

import http.server
import socketserver

PORT = 8080

handler = http.server.SimpleHTTPRequestHandler

with socketserver.TCPServer(("localhost", PORT), handler) as httpd:
    print("Handling Requests for port: ", PORT)
    httpd.serve_forever()