#!/usr/bin/env python3

import gpiozero 

def button_pressed ():
    print("Button Pressed")
    led.blink(on_time=0.5, off_time=0.5, background=False, n=1)

led = gpiozero.PWMLED(17,)
led.blink(on_time=0.02, off_time=0.01, n=10 )

gpiozero.Button(16).when_activated = button_pressed

gpiozero.Button(20).wait_for_active()