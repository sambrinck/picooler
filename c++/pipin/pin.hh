#ifndef PIN_HH
#define PIN_HH

namespace pi {
class Pin {
    public:
        Pin(int pin);

        bool getValue() const;
        int getPin() const {return pin;}

    protected:
      int pin = 0;

    private:  
      static bool setup;

};
}

#endif