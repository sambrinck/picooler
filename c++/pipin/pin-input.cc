#include "pin-input.hh"

#include  <wiringPi.h>

#include <iostream>

namespace {
    static pi::PinIn* pins[30];
    void (* callBack [30])();
}

pi::PinIn::PinIn(int pin) :
    Pin(pin) {

        pins[pin] = this;
        if (callBack[1] == nullptr) {
            callBack[1]  = [](){pins[1]->listen(); };
            callBack[2]  = [](){pins[2]->listen(); };
            callBack[3]  = [](){pins[3]->listen(); };
            callBack[4]  = [](){pins[4]->listen(); };
            callBack[5]  = [](){pins[5]->listen(); };
            callBack[6]  = [](){pins[6]->listen(); };
            callBack[7]  = [](){pins[7]->listen(); };
            callBack[8]  = [](){pins[8]->listen(); };
            callBack[9]  = [](){pins[9]->listen(); };
            callBack[10] = [](){pins[10]->listen(); };
            callBack[11] = [](){pins[11]->listen(); };
            callBack[12] = [](){pins[12]->listen(); };
            callBack[13] = [](){pins[13]->listen(); };
            callBack[14] = [](){pins[14]->listen(); };
            callBack[15] = [](){pins[15]->listen(); };
            callBack[16] = [](){pins[16]->listen(); };
            callBack[17] = [](){pins[17]->listen(); };
            callBack[18] = [](){pins[18]->listen(); };
            callBack[19] = [](){pins[19]->listen(); };
            callBack[20] = [](){pins[20]->listen(); };
            callBack[21] = [](){pins[21]->listen(); };
            callBack[22] = [](){pins[22]->listen(); };
            callBack[23] = [](){pins[23]->listen(); };
            callBack[24] = [](){pins[24]->listen(); };
            callBack[25] = [](){pins[25]->listen(); };
            callBack[26] = [](){pins[26]->listen(); };
            callBack[27] = [](){pins[27]->listen(); };
            callBack[28] = [](){pins[28]->listen(); };
            callBack[29] = [](){pins[29]->listen(); };
            callBack[30] = [](){pins[30]->listen(); };
        }
        pinMode(pin, INPUT);
        wiringPiISR(pin, INT_EDGE_BOTH, callBack[pin]);

    }
//=================================================================
// Generic Listener
//=================================================================
void pi::PinIn::listen() {
    if (getValue()) {
        if (pinOn) pinOn(pin);
    } else {
        if (pinOff) pinOff(pin);
    }
}
