#include "pin-rgb.hh"

pi::PinRGB::PinRGB(int rPin,
                   int gPin,
                   int bPin):
    red(rPin),
    green(gPin),
    blue(bPin)
{

}

void pi::PinRGB::setValue(short rVal,
                          short gVal,
                          short bVal) {
    red.setPwm(255-rVal,   255);
    green.setPwm(255-gVal, 255);
    blue.setPwm(255-bVal,  255);
}
