#ifndef OPIN_HH
#define OPIN_HH

#include "pin.hh"

namespace pi {

class PinOut : public Pin {
public:
    PinOut(int pin);

    void setValue(bool value);
    void setPwm(int value, short range = 100);
    void toggle();
    void breathe();

    void operator = (bool value) {setValue(value);}
};
}

#endif
