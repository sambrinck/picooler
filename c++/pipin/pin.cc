#include "pin.hh"

#include <wiringPi.h>
#include <iostream>

bool pi::Pin::setup = false;

pi::Pin::Pin(int pin) :
    pin(pin) {

    if (not setup) {
        wiringPiSetupGpio();
        //wiringPiSetup();
    }
    
}

bool pi::Pin::getValue() const {
    return digitalRead(pin);
}
