#include "pin-output.hh"

#include <wiringPi.h>
#include <softPwm.h>

#include <iostream>
#include <chrono>
#include <thread>

pi::PinOut::PinOut(int pin):
  Pin(pin) {
    pinMode(pin, OUTPUT);
}

void pi::PinOut::setValue(bool value) {
    //pinMode(pin, OUTPUT);
    std::cout << "Turning pin " << pin;
    if (not value) {
        std::cout << " on\n";
        digitalWrite (pin, HIGH);
    } else {
        std::cout << " off\n";
        digitalWrite (pin, LOW);
    }
}

void pi::PinOut::setPwm(int value, short range) {
  softPwmCreate(pin,0,100);
  std::cout << "Pin " << pin << " dim to " << value << "\n";
  softPwmWrite(pin, value);
}

void pi::PinOut::toggle() {
    setValue(not getValue());
}

void pi::PinOut::breathe() {
  // Anything above about 255 seems to cause flashing
  static constexpr int MAX_VALUES=255;
  //pinMode(pin, PWM_OUTPUT);
  softPwmCreate(pin,0,MAX_VALUES);
  std::cout << "Pin " << pin << " breathing up\n";
  for (int i=0; i<MAX_VALUES; ++i) {
    softPwmWrite(pin, i);
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
  }
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  std::cout << "Pin " << pin << " breathing down\n";
  for (int i=MAX_VALUES; i>0; --i) {
    softPwmWrite(pin, i);
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
  }
  std::this_thread::sleep_for(std::chrono::milliseconds(500));
}
