#ifndef RGB_PIN_HH
#define RGB_PIN_HH

#include "pin-output.hh"

namespace pi
{

class PinRGB
{
public:
    PinRGB(int rPin,
           int gPin,
           int bPin);

    void setValue(short rVal,
                  short gVal,
                  short bVal);

private : 
    PinOut red;
    PinOut green;
    PinOut blue;
    static constexpr const short RGB_RANGE=255;
};
} // namespace pi

#endif