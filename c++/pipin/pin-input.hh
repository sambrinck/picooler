#ifndef IPIN_HH
#define IPIN_HH

#include "pin.hh"

#include <functional>

namespace pi {

class PinIn : public Pin {
public:
    PinIn(int pin);

    void listen();
    void setOnListener( std::function<void(int)> listener) {pinOn = listener;}
    void setOffListener(std::function<void(int)> listener) {pinOff= listener;}

private:
    std::function<void(int)> pinOn;
    std::function<void(int)> pinOff;
};
}

#endif