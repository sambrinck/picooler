
#include "condition-variable.hh"



ConditionVariable::ConditionVariable(bool lock) {
    this->lock(lock);
}

void ConditionVariable::lock(bool locked) {
    this->locked = locked;
    if (not locked) {
       notify_all();
    }
}

// template <> void ConditionVariable::waitFor(const std::chrono::seconds&  duration) ;
// template <> void ConditionVariable::waitFor(const std::chrono::minutes&  duration) ;
// template <class Rep, class Period>
// void ConditionVariable::waitFor(const std::chrono::duration<Rep,Period>&  duration) {
//     std::unique_lock<std::mutex> lock(mutex);
//     while (locked) cv.wait_for(duration);
// }

void ConditionVariable::wait() {
    std::unique_lock<std::mutex> lock(mutex);
    condition_variable::wait(lock, [&](){ return not locked;});
}

void ConditionVariable::operator = (bool value) {
    std::unique_lock<std::mutex> lock(mutex);
    locked = value;
    notify_all();
}

