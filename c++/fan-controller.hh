#ifndef FAN_CONTROLLER_HH
#define FAN_CONTROLLER_HH

#include "pipin/pin-output.hh"

#include <vector>

class FanController {
public:
    FanController();
    enum Speed {
        HIGH = 0,
        MEDIUM,
        LOW,
        OFF,
    };  

    void setSpeed(Speed speed);
private:
    std::vector<pi::PinOut> pins;
};

#endif