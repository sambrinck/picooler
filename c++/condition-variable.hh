#ifndef CONDITION_VARIABLE_HH
#define CONDITION_VARIABLE_HH

#include <atomic>
#include <chrono>
#include <mutex>
#include <condition_variable>

class ConditionVariable : protected std::condition_variable  {
public:
    explicit ConditionVariable(bool lock = true);
    ~ConditionVariable() {unlock();}


    void lock(bool locked = true);
    void unlock() {lock(false);}

    template <class Rep, class Period>
    void waitFor(const std::chrono::duration<Rep,Period>&  waitTime) {
        std::unique_lock<std::mutex> lock(mutex);

        // This was totally broken 
        wait_for(lock, waitTime, [&] { return not locked; });
    }
    void wait();

    operator bool() {return locked;}
    void operator = (bool value);
    
private:
    std::mutex mutex;
    std::atomic<bool> locked = true;
};

#endif