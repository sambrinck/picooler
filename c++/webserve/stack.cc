#include <thread>
#include <memory>
#include <chrono>
#include <cstdlib>
#include <restbed>
#include <iostream>

using namespace std;
using namespace restbed;

void get_method_handler( const shared_ptr< Session > session )
{
  for (const auto& key : session->get_headers() )
  {
    std::cout << "get Method Handler: " << key.first << std::endl;
    std::cout << "get Method Handler: " << key.second << std::endl;
  }

  for (const std::string& key : session->keys() )
  {
    std::cout << "get Method Handler: " << key << std::endl;

  }

  const auto& request (session->get_request());

  std::cout << "get Method Handler: " << __LINE__ << " " << request->get_body() << std::endl;
  std::cout << "get Method Handler: " << __LINE__ << " " << session->get_destination() << std::endl;
  std::cout << "get Method Handler: " << __LINE__ << " " << session->get_request() << std::endl;
  std::cout << "get Method Handler: " << __LINE__ << " " << session->get_origin() << std::endl;
  std::cout << "get Method Handler: " << __LINE__ << " " << session->get_id() << std::endl;
    session->close( OK, "Hello, World!", { { "Content-Length", "13" }, { "Connection", "close" } } );
}

void service_ready_handler( Service& )
{
    fprintf( stderr, "Hey! The service is up and running.\n" );
}

int main( const int, const char** )
{
    auto resource = make_shared< Resource >( );
    resource->set_path( "/.*" );
    resource->set_method_handler( "GET", get_method_handler );

    auto settings = make_shared< Settings >( );
    settings->set_port( 1984 );

    auto service = make_shared< Service >( );
    service->publish( resource );
    service->set_ready_handler( service_ready_handler );
    service->start( settings );

    return EXIT_SUCCESS;
}
