#include "colorpicker.h"
#include <QApplication>
#include <QColorDialog>
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    ColorPicker w;
    w.show();

    return a.exec();
}

//int main(int argc, char *argv[])
//{
//    QApplication a(argc, argv);

//    /* setup a quick and dirty window */
//    QMainWindow app;
//    app.setGeometry(250, 250, 600, 400);

//    QColorDialog *colorDialog = new QColorDialog(&app);
//    /* set it as our widiget, you can add it to a layout or something */
//    app.setCentralWidget(colorDialog);
//    /* define it as a Qt::Widget (SubWindow would also work) instead of a dialog */
//    colorDialog->setWindowFlags(Qt::Widget);
//    /* a few options that we must set for it to work nicely */
//    colorDialog->setOptions(
//                /* do not use native dialog */
//                QColorDialog::DontUseNativeDialog
//                /* you don't need to set it, but if you don't set this
//                    the "OK" and "Cancel" buttons will show up, I don't
//                    think you'd want that. */
//                | QColorDialog::NoButtons
//    );

//    app.show();
//    return a.exec();
//}
