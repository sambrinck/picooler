#include "colorpicker.h"
#include "ui_colorpicker.h"

#include <qcolordialog.h>

#include <iostream>


ColorPicker::ColorPicker(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ColorPicker),
    rgb(0,1,2)
{
    ui->setupUi(this);
    QColorDialog* dialog = new QColorDialog(ui->centralWidget);
    setCentralWidget(dialog);
    dialog->setWindowFlags(Qt::Widget);
    dialog->setOptions(
                QColorDialog::DontUseNativeDialog
                | QColorDialog::NoButtons);

    connect(dialog, SIGNAL(currentColorChanged(const QColor&)),
            this, SLOT(colorChanged(const QColor&)));
//    dialog.open();
}

ColorPicker::~ColorPicker()
{
    delete ui;
}

void ColorPicker::colorChanged(const QColor &color)
{
    rgb.setValue(static_cast<short>(color.red()),
                 static_cast<short>(color.green()),
                 static_cast<short>(color.blue()));
    std::cout << __FILE__ << ":" << __LINE__ << ": Color: " << color.red() << std::endl;
}
