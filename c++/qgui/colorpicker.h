#ifndef COLORPICKER_H
#define COLORPICKER_H

#include <QMainWindow>
#include "pin-rgb.hh"

namespace Ui {
class ColorPicker;
}

class ColorPicker : public QMainWindow
{
    Q_OBJECT

public:
    explicit ColorPicker(QWidget *parent = nullptr);
    ~ColorPicker();

private slots:
    void colorChanged(const QColor& color);
private:
    Ui::ColorPicker *ui;
    pi::PinRGB rgb;
};

#endif // COLORPICKER_H
