#include "arg-parser.hh"

#include <sstream>
#include <algorithm>

namespace {
}

/***********************************************************
 * Initialize the list of variables
 ***********************************************************/
ArgParser::ArgParser(int argc, const char* argv[]):
    argList(argv+1,argv+argc)
{
}

/***********************************************************
 * Get a flag from the list
 ***********************************************************/
bool ArgParser::getFlag(const std::string& flag, const std::string prefix) {
    auto iter = std::find(argList.begin(), 
                        argList.end(), 
                        prefix+flag);
    if (argList.end() != iter ) {
        argList.erase(iter);
        return true;
    }
    return false;
}

/***********************************************************
 * Get an argument from the list
 ***********************************************************/
template std::optional<std::string> ArgParser::getArg(const std::string &options, const std::string prefix);
template std::optional<double>      ArgParser::getArg(const std::string &options, const std::string prefix);
template std::optional<int>         ArgParser::getArg(const std::string &options, const std::string prefix);
template std::optional<short>       ArgParser::getArg(const std::string &options, const std::string prefix);
template<class TYPE>
std::optional<TYPE> ArgParser::getArg(const std::string &option, const std::string prefix) {

    auto iter = std::find(argList.begin(), 
                        argList.end(), 
                        prefix+option);
    std::optional<TYPE> retval;
    if (iter == argList.end()) {
        return retval;
    }

    retval=TYPE(); //initialize the optional

    argList.erase(iter++); // remove the --arg from list
    std::istringstream (*iter) >> *retval;
    argList.erase(iter); // remove the argument value from list

    return retval;
}


// Temporary driver for quick testing
// compile with g++ -std=c++17 arg-parser.cc -o argParser
// #include <iostream>
// int main(int argc, const char** argv) {
//   ArgParser argParser(argc, argv);

//   while (std::optional<int> arg = argParser.getArg<int>("int")) {
//     std::cout << "Int found: " << *arg << std::endl;
//   }

//   while (std::optional<double> arg = argParser.getArg<int>("double")) {
//     std::cout << "Double found: " << *arg << std::endl;
//   }
//   return 0;
// }
