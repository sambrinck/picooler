#ifndef GET_ARG_HH
#define GET_ARG_HH

#include <string>
#include <optional>
#include <list>

class ArgParser {

public:
  /***********************************************************
   * Initialize the list of variables
   ***********************************************************/
  ArgParser(int argc, const char* argv[]);

  /***********************************************************
   * Get a flag from the list
   ***********************************************************/
  bool getFlag(const std::string& flag, const std::string prefix = "-");

  /***********************************************************
   * Get an argument from the list
   ***********************************************************/
  template<class TYPE>
    std::optional<TYPE> getArg(const std::string &option, const std::string prefix = "--");

  // Add a line like this for each type of argument you want to to retreive 
  // (can be added in file that uses it)
  //template int getArg(const std::string &option, int def);

private:
  std::list<std::string> argList;
};

#endif
