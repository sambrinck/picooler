#include "fan-controller.hh"
#include "pins.hh"

FanController::FanController() {
    pins.assign(Speed::HIGH,   pi::PinOut(pi::PINS::FAN_HIGH));
    pins.assign(Speed::MEDIUM, pi::PinOut(pi::PINS::FAN_MEDIUM));
    pins.assign(Speed::LOW,    pi::PinOut(pi::PINS::FAN_LOW));
}

void FanController::setSpeed(Speed speed) {
    pins[Speed::HIGH].setValue(speed == Speed::HIGH);
    pins[Speed::MEDIUM].setValue(speed == Speed::MEDIUM);
    pins[Speed::LOW].setValue(speed == Speed::LOW);
}