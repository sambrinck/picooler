#!/bin/bash

pin-type() {
  PIN=$1
  return "$PIN" -eq "$PIN" &> /dev/null  &&
      (( "$PIN" < 28 )) &&
      (( "$PIN" > 2 ))
}

cleanup() {
  run=false
  #echo $PIN > /sys/class/gpio/unexport
}

PIN=$1

if ! pin-type $PIN > /dev/null ; then
    echo "$PIN" is not a number
fi

while ! pin-type $PIN > /dev/null ; do
  read -p "Please enter a valid pin #: " PIN
done
    
echo flashing pin $PIN

echo $PIN > /sys/class/gpio/export
echo out > /sys/class/gpio/gpio$PIN/direction
ls /sys/class/gpio/gpio$PIN
trap cleanup sigint

run=true

while $run; do
  sleep 1
  echo pin $PIN ON
  echo 1 > /sys/class/gpio/gpio$PIN/value
  sleep 1
  echo pin $PIN OFF
  echo 0 > /sys/class/gpio/gpio$PIN/value
done                         

echo $PIN > /sys/class/gpio/unexport

