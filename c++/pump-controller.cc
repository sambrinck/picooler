
#include "pump-controller.hh"
#include "pins.hh"


#include <sys/stat.h> // For mkdir
#include <fstream>
#include <string>
#include <iostream>
#include <thread>

// std::cout << __FILE__ << ":" << __LINE__ << std::endl;

namespace {
    static const std::string PUMP_RUN_TIME_FILE ("/etc/pi/pump.time");
    std::chrono::seconds getRuntime();
    void setRuntime(const std::chrono::seconds& runtime);

    std::chrono::seconds getRuntime() {
        int runtime = 0;
        std::ifstream timeFile(PUMP_RUN_TIME_FILE);
        timeFile >> runtime;
        // std::cout << "Wrote " << runtime << " to file: " << PUMP_RUN_TIME_FILE << std::endl;
        return std::chrono::seconds(runtime);
    }

    void setRuntime(const std::chrono::seconds& runtime) {
        // std::cout << "Read " << runtime.count() << " from file: " << PUMP_RUN_TIME_FILE << std::endl;
        std::ofstream (PUMP_RUN_TIME_FILE, std::ios_base::trunc) << runtime.count();
    }
}

/***********************************************************
 * Constuctor
 ***********************************************************/
PumpController::PumpController() :
    mainPump(pi::PINS::MAIN_PUMP),
    purgePump(pi::PINS::PURGE_PUMP),
    purgeDelay(std::chrono::hours(8)),
    purgeLength(std::chrono::seconds(10))
{
    mkdir("/etc/pi", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
}

/***********************************************************
 * Destuctor
 ***********************************************************/
PumpController::~PumpController() {
    std::cout << __FILE__ << ":" << __LINE__ << std::endl;
    cv = false;
    stopPump();
}

/***********************************************************
 * Start the Main Pump
 ***********************************************************/
void PumpController::startPump() {
    pumpStarted = std::chrono::system_clock::now();
    mainPump.setValue(true);

    // Start the thread to start the puge pump after the correct ammount of time
    std::thread([&](){ 
        std::chrono::seconds runtime = getRuntime();
        std::cout << __FILE__ << __LINE__ << " Sleeping for " 
                  << std::chrono::duration_cast<std::chrono::seconds>(purgeDelay - runtime).count() 
                  << " Seconds.\n";
        
        cv.waitFor(purgeDelay - runtime);
            std::cout << __FILE__ << __LINE__ << " PURGING!!!\n";
            // It is time to start the purge pump
        }).detach();
}

/***********************************************************
 * Stop the Main Pump
 ***********************************************************/
void PumpController::stopPump() {
    using namespace std::chrono;

    std::cout << __FILE__ << __LINE__ << " STOPPING!!!\n";

    mainPump.setValue(false);
    purgePump.setValue(false);

    setRuntime( getRuntime() + 
            duration_cast<seconds>(system_clock::now() - pumpStarted));
}

