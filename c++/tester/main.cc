
#include "pipin/pin-input.hh"
#include "pipin/pin-output.hh"
#include "pipin/pin-rgb.hh"

#include "arg-parser/arg-parser.hh"

#include <list>
#include <vector>

#include <iostream>
#include <thread>
#include <chrono>
#include <memory>
#include <stddef.h>

// std::cout << __FILE__ << ":" <<  __LINE__ << std::endl;

#define  LOG(value)  std::cout << __FILE__ << ":" <<  __LINE__ << ": " << value << std::endl;

class Button {
public:
    Button(std::vector<pi::PinOut>& pins,
           pi::PinIn& greenPin,
           pi::PinIn& bluePin,
           int sleepTime) :
        oPins(pins),
        sleepTime(sleepTime)
    { 
        std::cout << "Green is " << greenPin.getPin() << std::endl;
        std::cout << "Blue is " << bluePin.getPin() << std::endl;
        bluePin .setOnListener([this](int){ LOG("BLUE ON"); bluePressed ();});
        bluePin .setOffListener([this](int){ LOG("BLUE OFF"); greenPressed ();});
        greenPin.setOnListener([this](int){ LOG("GREEN ON"); greenPressed();});
        greenPin.setOffListener([this](int){ LOG("GREEN OFF"); bluePressed();});
    }

    void greenPressed() {
        // std::cout << "Green Button pressed" << std::endl;
        for (pi::PinOut& pin : oPins) {
            pin = true;
            std::this_thread::sleep_for(std::chrono::milliseconds(sleepTime));
            pin = false;
            // std::this_thread::sleep_for(std::chrono::milliseconds(sleepTime));
        }
    }
    void bluePressed() {
        // std::cout << "Blue Button pressed" << std::endl;
        for (int i = oPins.size()-1; i >= 0; --i) {
        // for (pi::PinOut& pin : oPins) {
            oPins[i] = true;
            std::this_thread::sleep_for(std::chrono::milliseconds(sleepTime));
            oPins[i] = false;
            // std::this_thread::sleep_for(std::chrono::milliseconds(sleepTime));
        }
    }

private:
    std::vector<pi::PinOut>& oPins;
    const int sleepTime;
};


void barGraph(std::list<pi::PinIn>&    iPins,
              std::vector<pi::PinOut>& oPins,
              int sleepTime) {

    // oPins[0].breathe();
    
    for (;;) {
        //std::this_thread::sleep_for(std::chrono::milliseconds(sleepTime));

    //    // Make all the lights breathe at the same time
    //    std::list<std::thread> threads;
    //    for (pi::PinOut& pin : oPins) {
    //      threads.emplace_back([&pin]() { pin.breathe(); });
    //      std::this_thread::sleep_for(std::chrono::milliseconds(sleepTime));
    //    }
    //    for (std::thread& thread : threads) {
    //      thread.join();
    //    }

    //  // Set the power level to the specified brightness
        // while (std::optional<int> level = argParser.getArg<int>("level")) {
        //   for (pi::PinOut& pin : oPins) {
        //     pin.setPwm(level ? *level : 70);
        //   }
        //   std::this_thread::sleep_for(std::chrono::milliseconds(sleepTime));
        // }

        for (pi::PinOut& pin : oPins) {
            pin = false;
        }

        for (pi::PinOut& pin : oPins) {
          pin = true;
          std::this_thread::sleep_for(std::chrono::milliseconds(sleepTime));
          pin = false;
        }

        for (int i = oPins.size()-1; i >= 0; --i) {
          oPins[i] = true;
          std::this_thread::sleep_for(std::chrono::milliseconds(sleepTime));
          oPins[i] = false;
        }
//        for (pi::PinOut& pin : oPins) {
//            //pin.toggle();
//            pin.breathe();
//        }
    }
}


/***********************************************************
 * Main
 ***********************************************************/
int main(int argc, char const *argv[])
{
    ArgParser argParser(argc, argv);
    std::vector<pi::PinIn*> iPins;
    // pi::PinIn *iPins[2];
    std::vector<pi::PinOut> oPins;

    while (std::optional<int> pin = argParser.getArg<int>("out")) {
        std::cout << "Adding Output Pin: " << *pin << std::endl;
        oPins.emplace_back(*pin);
    }

    size_t count=0;
    while (std::optional<int> pin = argParser.getArg<int>("in")) {
        std::cout << "Adding Input Pin: " << *pin << std::endl;
        iPins.push_back( new pi::PinIn(*pin));
    }

    std::optional<int> sleep = argParser.getArg<int>("sleep");
    int sleepTime = (sleep ? *sleep : 500);
   
   // Start with all the pins off
    for (pi::PinOut& pin : oPins) {
      pin = false;
    }

    for (const pi::PinIn* pin : iPins) {
        LOG(pin->getPin());
    }

    pi::PinRGB rgb(oPins[0].getPin(),
                   oPins[1].getPin(),
                   oPins[2].getPin());

    rgb.setValue(*argParser.getArg<short>("red"),
                 *argParser.getArg<short>("green"),
                 *argParser.getArg<short>("blue"));

    // barGraph(iPins, oPins, sleepTime);
    for (;;) {
          std::this_thread::sleep_for(std::chrono::milliseconds(sleepTime));
    }
    return 0;
}
