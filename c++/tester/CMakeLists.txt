cmake_minimum_required (VERSION 2.6)
PROJECT(tester)
FILE (GLOB sources *.cc)

message(STATUS "tester INCLUDE: ${CMAKE_SOURCE_DIR}/pipin")


add_executable (tester ${sources})
target_include_directories(tester PUBLIC "${CMAKE_SOURCE_DIR}" )
target_link_libraries (tester "${CMAKE_THREAD_LIBS_INIT}" )
target_link_libraries (tester pipin argParser)
