
#include "pipin/pin-input.hh"
#include "pipin/pin-output.hh"

#include "arg-parser/arg-parser.hh"
#include "pump-controller.hh"

#include <list>

#include <iostream>
#include <thread>
#include <chrono>

// std::cout << __LINE__ << std::endl;

/***********************************************************
 * Main
 ***********************************************************/
int main(int argc, char const *argv[])
{
    using std::list;
    using std::string;
    using std::optional;
    
    ArgParser argParser(argc, argv);

    list<pi::PinIn> iPins;
    list<pi::PinOut> oPins;
    while (optional<int> pin = argParser.getArg<int>("in")) {
        std::cout << "Adding Input Pin: " << *pin << std::endl;
        iPins.emplace_back(*pin);
        iPins.back().setOnListener([](int pin) {
            std::cout << pin << " Has turned on\n";
        });
        iPins.back().setOffListener([](int pin) {
            std::cout << pin << " Has turned off\n";
        });
    }
// /home/sam/picooler/main.cc:26:63: note:   cannot convert ‘list’ (type ‘
    //   std::__cxx11::list<      std::__cxx11::basic_string<char> >’) to type ‘
// const std::__cxx11::list<const std::__cxx11::basic_string<char> >&’



    while (std::optional<int> pin = argParser.getArg<int>("out")) {
        std::cout << "Adding Output Pin: " << *pin << std::endl;
        oPins.emplace_back(*pin);
    }

    PumpController pumpController;
    pumpController.startPump();

    for (;;) { 
        // std::this_thread::sleep_for(std::chrono::seconds(1));
        std::this_thread::sleep_for(std::chrono::seconds(2));
        // pumpController.stopPump();
        for (pi::PinOut& pin : oPins) {
            pin.toggle();
        }
    }
    return 0;
}
