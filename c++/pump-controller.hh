#ifndef PUMP_CONTROLLER_HH
#define PUMP_CONTROLLER_HH

#include "pipin/pin-output.hh"
#include "condition-variable.hh"

#include <chrono>

class PumpController {
public:
    PumpController();
    ~PumpController();

    void startPump();
    void stopPump();

private:
    pi::PinOut mainPump;
    pi::PinOut purgePump;

    std::chrono::system_clock::time_point pumpStarted;

    std::chrono::seconds purgeDelay;
    std::chrono::seconds purgeLength;

    ConditionVariable cv;

};
    
#endif