#ifndef PINS_HH
#define PINS_HH

namespace pi{

    enum PINS {
        FAN_LOW = 1,
        FAN_MEDIUM = 2,
        FAN_HIGH = 3,
        MAIN_PUMP = 4,
        PURGE_PUMP = 5

    };
};

#endif